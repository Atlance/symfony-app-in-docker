#!/bin/bash
set -a

git fetch origin -q

current_branch=$(git branch | grep \* | cut -d ' ' -f2)
changed_files=$(git diff ${current_branch} --first-parent --name-only --diff-filter=MARC | grep '.php$')

if [ $? -eq 0 ] ; then
    changed_files=$(echo ${changed_files} | xargs ls -1 2>/dev/null)
    if [ -n '${changed_files}' ]; then
        vendor/bin/phpstan analyse -c phpstan.neon ${changed_files} --level max --no-progress
    fi
fi
