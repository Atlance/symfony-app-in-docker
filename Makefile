up: docker-up
down: docker-down
restart: docker-down docker-up
init: docker-down-clear docker-pull docker-build docker-up app-init
inspect: app-phpcs-fix app-analyse

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

app-init: app-composer-install app-assets-install app-oauth-keys app-wait-db app-migrations-migrate

app-composer-install:
	docker-compose run --rm app-php-cli composer install

app-assets-install:
	docker-compose run --rm app-node yarn install
	docker-compose run --rm app-node npm rebuild node-sass

app-oauth-keys:
	docker-compose run --rm app-php-cli mkdir -p var/oauth
	docker-compose run --rm app-php-cli openssl genrsa -out var/oauth/private.key 2048
	docker-compose run --rm app-php-cli openssl rsa -in var/oauth/private.key -pubout -out var/oauth/public.key
	docker-compose run --rm app-php-cli chmod 644 var/oauth/private.key var/oauth/public.key

app-wait-db:
	until docker-compose exec -T app-postgres pg_isready --timeout=0 --dbname=app ; do sleep 1 ; done

app-migrations-migrate:
	docker-compose run --rm app-php-cli php bin/console doctrine:migrations:migrate --no-interaction --all-or-nothing --allow-no-migration

app-migrations-diff:
	docker-compose run --rm app-php-cli php bin/console doctrine:migrations:diff --no-interaction

app-analyse:
	scripts/phpstan.sh

app-phpcs-fix:
	scripts/phpcs-fixer.sh
